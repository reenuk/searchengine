﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SearchResult.Classes;

namespace SearchEngine.Classes
{
    public class GoogleSearch : EngineSearch
    {
        private const string GoogleApiString = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&rsz=large&safe=active&q={0}&start={1}";
        public GoogleSearch()
        {
            if (ApiUri == null)
            {
                ApiUri = GoogleApiString;
            }
        }
        public override List<SearchItem> Search(string searchExpression)
        {
            CheckWebsiteIsLive();
            var resultsList = new List<SearchItem>();

            if (Thread.CurrentThread.ThreadState == ThreadState.Running || Thread.CurrentThread.ThreadState == ThreadState.Background)
            {
                for (int i = 0; i <= 56; i += 8)
                {
                    var searchUrl = ValidUri(searchExpression, i);
                    var page = new WebClient().DownloadString(searchUrl);
                    var res = (JObject) JsonConvert.DeserializeObject(page);
                    var resultsQuery =
                        res["responseData"]["results"].Children().Select(result => result != null
                                                                                       ? new SearchItem
                                                                                           {
                                                                                               Url =
                                                                                                   result.Value<string>(
                                                                                                       "url"),
                                                                                               Title =
                                                                                                   result.Value<string>(
                                                                                                       "title"),
                                                                                               Content =
                                                                                                   result.Value<string>(
                                                                                                       "content"),
                                                                                               Engine =
                                                                                                   SearchItem
                                                                                             .FindingEngine
                                                                                             .Google
                                                                                           }
                                                                                       : new SearchItem());
                    resultsList.AddRange(resultsQuery);
                }
            }
            else
            {
                Thread.CurrentThread.Abort();
            }
            return resultsList;
        }
    }
}