﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Services.Protocols;
using SearchEngine.BingApiService;
using SearchResult.Classes;

namespace SearchEngine.Classes
{
    public class BingSearch : EngineSearch
    {
        private const string BingApiString = "http://api.bing.net/search.wsdl";

        public BingSearch()
        {
            if (ApiUri == null)
            {
                ApiUri = BingApiString;
            }
        }

        public override List<SearchItem> Search(string searchExpression)
        {
            CheckWebsiteIsLive();
            var resultsList = new List<SearchItem>();
            
            var serv = new BingService();
            var req = new SearchRequest
                {
                    AppId = "AppId",
                    Query = searchExpression,
                    Sources = new[] {SourceType.Web}
                };
            SearchResponse response = null;

            try
            {
                response = serv.Search(req);
            }
            catch (SoapException ex)
            {
                Debug.Write(ex.Message + " " + ex.InnerException);
                return null;
            }

            if(response != null)
            {
                if (response.Web != null)
                {
                    if (response.Web.Results != null)
                    {
                        resultsList = response.Web.Results.Select(result => new SearchItem
                            {
                                Content = result.Description,
                                Engine = SearchItem.FindingEngine.Bing,
                                Title = result.Title,
                                Url = result.Url
                            }).ToList();
                    }
                }
            }
            return resultsList;
        }
    }
}


