﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SearchResult.Classes
{
    public struct SearchItem
    {
        public string Url;
        public string Title;
        public string Content;
        public FindingEngine Engine;
        public enum FindingEngine { Google, Bing};
    }
}
