﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using SearchResult.Classes;

namespace SearchEngine.Classes
{
    public abstract class EngineSearch
    {
        private string _apiUri;
        public string ApiUri
        {
            get
            {
                return _apiUri;
            }

            set
            {
                var searchUrl = new Uri(value);
                try
                {
                    Uri.CheckHostName(searchUrl.Host);
                }
                catch (Exception ex)
                {
                    Debug.Write("Invalid Uri");
                }
                _apiUri = value;
            }
        }
        public abstract List<SearchItem> Search(string searchExpression);
        public bool CheckWebsiteIsLive()
        {
            bool status = false;
            try
            {
                var myRequest = (HttpWebRequest)WebRequest.Create(ApiUri);
                var response = (HttpWebResponse)myRequest.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                Debug.Write(string.Format("{0} is unavailable: {1}", ApiUri, ex.Message));
            }
            return status;
        }
        public Uri ValidUri(string searchExpression, int i)
        {
            if (searchExpression == null)
            {
                throw new ArgumentNullException("searchExpression");
            }
            var searchUrl = new Uri(string.Format(ApiUri, searchExpression, i));
            try
            {
                Uri.CheckHostName(searchUrl.Host);
            }
            catch (Exception ex)
            {
                Debug.Write("Invalid Uri");
            }
            return searchUrl;
        }
    }
}