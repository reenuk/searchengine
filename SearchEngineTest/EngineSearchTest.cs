﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SearchEngine.Classes;

namespace SearchEngineTest
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class EngineSearchTest
    {
        public TestContext TestContext { get; set; }

        static EngineSearch engineSearch = new GoogleSearch();
        // public static EngineSearch engineSearch = new BingSearch(); 

        [TestMethod]
        public void AssigningValidUriDoesNotThrowExceptionTest()
        {
            engineSearch.ApiUri = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&rsz=large&safe=active";
        }

        [TestMethod]
        [ExpectedException(typeof (UriFormatException))]
        public void AssigningInvalidUriThrowsException()
        {
            engineSearch.ApiUri = "ajax.googleapis.com/ajax/services/search/web?v=1.0&rsz=large&safe=active";
        }

        [TestMethod]
        public void CheckWebsiteIsLiveTest()
        {
            Assert.IsTrue(engineSearch.CheckWebsiteIsLive());
        }

        [TestMethod]
        public void GoogleApiReturnsSomeResultTest()
        {
            Assert.IsNotNull(engineSearch.Search("asos"));
        }

        [TestMethod]
        public void GoogleApiSearchNotBlockingThreadsTest()
        {
            engineSearch.Search("");
            Assert.AreEqual(Thread.CurrentThread.ThreadState,  ThreadState.Running);
        }
    }
}


    

